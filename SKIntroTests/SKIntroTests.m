//
//  SKIntroTests.m
//  SKIntroTests
//
//  Created by Hanley Weng on 27/09/13.
//  Copyright (c) 2013 Hanley Weng. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SKIntroTests : XCTestCase

@end

@implementation SKIntroTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
