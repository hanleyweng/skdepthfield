//
//  SKIntroAppDelegate.h
//  SKIntro
//
//  Created by Hanley Weng on 27/09/13.
//  Copyright (c) 2013 Hanley Weng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKIntroAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
