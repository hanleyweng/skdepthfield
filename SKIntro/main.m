//
//  main.m
//  SKIntro
//
//  Created by Hanley Weng on 27/09/13.
//  Copyright (c) 2013 Hanley Weng. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SKIntroAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SKIntroAppDelegate class]));
    }
}
