//
//  SKIntroMyScene.m
//  SKIntro
//
//  Created by Hanley Weng on 27/09/13.
//  Copyright (c) 2013 Hanley Weng. All rights reserved.
//

#import "SKIntroMyScene.h"

#define maxZdistance ((float) 15.0)
#define minZdistance ((float) 0.0)

@implementation SKIntroMyScene

@synthesize layers;

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        
        int numLayers = 10;
        
        float rx = self.frame.size.width/2;
        float ry = self.frame.size.height/2;
        float rr = 10; // corner width
        
        float positionRadius = 300;
        float positionAngleIteration = 30 * M_PI / 180;
        float positionAngleInitial = 0;

        layers = [[NSMutableArray alloc] init];
        
        for (int i=0; i<numLayers; i++) {
            SKShapeNode* layer = [SKShapeNode node];
            layer.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
            
            CGPathRef rectPath = CGPathCreateWithRoundedRect((CGRect){ { -rx, -ry }, { rx * 2, ry * 2 } }, rr,rr, NULL);
            layer.path = rectPath;
            CGPathRelease(rectPath);
            
            // Calculate Position
            float angle = positionAngleIteration * i + positionAngleInitial;
            float x = cosf(angle)*positionRadius;
            float y = sinf(angle)*positionRadius;
            
            // Add Image Tile
            SKSpriteNode *tile = [SKSpriteNode spriteNodeWithImageNamed:@"cat.png"];
            tile.alpha = 1;
            tile.position = CGPointMake(x,y);
            [tile setScale:0.5f];
            [layer addChild:tile];
            
            
//            // Add debugTile
//            SKShapeNode *debugTile = [SKShapeNode node];
//            debugTile.position = CGPointMake(x,y);
//            float tr = self.frame.size.width/6;
//            tr = 50;
//            float trr = 5;
//            CGPathRef rectPath2 = CGPathCreateWithRoundedRect((CGRect){ { -tr, -tr }, { tr * 2, tr * 2 } }, trr,trr, NULL);
//            debugTile.path = rectPath2;
//            CGPathRelease(rectPath2);
//            [layer addChild:debugTile];
            
            
            // Use Distance - Set Initial scale
            float zPositionIn3dWorld = map(i, 0, numLayers, minZdistance, maxZdistance);
            
            float scale = [self getScaleForZDistance:zPositionIn3dWorld];
            [layer setScale:scale];
            
            // Load UserData for Layer
            layer.userData = [[NSMutableDictionary alloc] init];
            
            [layer.userData setObject:[NSNumber numberWithFloat:scale] forKey:@"depthz"];
            
            [layer.userData setObject:[NSNumber numberWithFloat:zPositionIn3dWorld] forKey:@"zPositionIn3dWorld"];
            
            [layers addObject:layer];
            [self addChild:layer];
        }
    }

    
    return self;
}

-(float) getScaleForZDistance:(float)zdistance {
    return 1.0/zdistance;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    // Diminish size gradually
    for (SKShapeNode *layer in layers) {
        
        float zPos3d = [[layer.userData valueForKey:@"zPositionIn3dWorld"] floatValue];

        
        float zoomSpeed = -0.03f;
        
        zPos3d+=zoomSpeed;
        if (zPos3d > maxZdistance) {
            zPos3d -= (maxZdistance-minZdistance);
        }
        if (zPos3d < minZdistance) {
            zPos3d += (maxZdistance-minZdistance);
        }
        
        // set z order - (could be optimized a bit here)
        layer.zPosition = -zPos3d;

        float scale = [self getScaleForZDistance:zPos3d];
        [layer setScale:scale];
        //[layer setAlpha: scale];
        
        [layer.userData setValue:[NSNumber numberWithFloat:zPos3d] forKey:@"zPositionIn3dWorld"];
    }
}

- (void)didMoveToView: (SKView *) view
{
    if (!self.contentCreated)
    {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents
{
    self.backgroundColor = [SKColor orangeColor];
    self.scaleMode = SKSceneScaleModeAspectFit;
//    [self addChild: [self newHelloNode]];
}


// static map function.
static float map(float value, float start1, float stop1, float start2, float stop2) {
    return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
}

@end
