//
//  SKIntroMyScene.h
//  SKIntro
//

//  Copyright (c) 2013 Hanley Weng. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKIntroMyScene : SKScene

@property BOOL contentCreated;

@property (nonatomic, retain) NSMutableArray* layers;

@end
